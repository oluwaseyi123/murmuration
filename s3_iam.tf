# create IAM User
resource "aws_iam_user" "newuser" {
  name = var.iamuser
}

# create IAM User group (Admin, Developer, ReadOnly or Monitoring)
resource "aws_iam_group" "new_admin_group" {
  name = var.admin_group
}

resource "aws_iam_group" "new_developer_group" {
  name = var.developer_group
}

resource "aws_iam_group" "new_readonly_group" {
  name = var.readonly_group
}


# add IAM User to IAM User group - Admin
resource "aws_iam_group_membership" "murmurationadmin" {
  name = "murmuration-group-membership"

  users = [
    aws_iam_user.newuser.name,
  ]

  group = aws_iam_group.new_admin_group.name
}


# add IAM User to IAM User group - Developer
resource "aws_iam_group_membership" "murmurationdeveloper" {
  name = "murmuration-group-membership"

  users = [
    aws_iam_user.newuser.name,
  ]

  group = aws_iam_group.new_developer_group.name
}


# add IAM User to IAM User group - ReadOnly
resource "aws_iam_group_membership" "murmurationreadonly" {
  name = "murmuration-group-membership"

  users = [
    aws_iam_user.newuser.name,
  ]

  group = aws_iam_group.new_readonly_group.name
}


# using data source to create IAM policy
data "aws_caller_identity" "current"{
}


# generate an IAM policy document for AWS data source
data "aws_iam_policy_document" "key_policy" {
statement {
sid = "allow root user permission"
effect = "Allow"
principals {
type = "AWS"
identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
}
actions = ["kms:*"]
resources = ["*"]
}
}


# generate an IAM policy document for s3 data source
data "aws_iam_policy_document" "s3_policy" {
  statement {
    effect = "Allow"
    actions = [
      "s3:ListAllMyBuckets"
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}"
    ]  
  }

  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject",
    ]

    resources = [
      "arn:aws:s3:::${var.bucket_name}/*"
    ]
  }
}

# IAM policy for s3 to implement the above IAM policy document
resource "aws_iam_policy" "execrise_policy" {
  name        = var.s3_policy_name
  description = "policy that will be used for s3"
  policy = data.aws_iam_policy_document.s3_policy.json
}

# attach IAM policy to User group - Admin
resource "aws_iam_group_policy_attachment" "admin_policy" {
  group      = aws_iam_group.new_admin_group.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}

# attach IAM policy to User group - Developer
resource "aws_iam_group_policy_attachment" "developer_policy" {
  group      = aws_iam_group.new_developer_group.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}

# attach IAM policy to User group - ReadOnly
resource "aws_iam_group_policy_attachment" "readonly_policy" {
  group      = aws_iam_group.new_readonly_group.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}
