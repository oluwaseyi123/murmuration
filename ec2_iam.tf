# data source - create IAM role policy document for ec2 instance with assumed role action
data "aws_iam_policy_document" "ec2_role_policy" {
  statement {
    effect = "Allow"
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# create IAM role and assign the policy document
resource "aws_iam_role" "execrise_iam_role" {
  name = "ec2Role"

  assume_role_policy = data.aws_iam_policy_document.ec2_role_policy.json

  tags = {
    name = "ec2Role"
  }
}

# attach the S3 policy to our IAM role
resource "aws_iam_role_policy_attachment" "attach" {
  role       = aws_iam_role.execrise_iam_role.name
  policy_arn = aws_iam_policy.execrise_policy.arn
}

