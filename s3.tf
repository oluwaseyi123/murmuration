# create s3 bucket with unique name

resource "aws_s3_bucket" "b" {
  bucket = var.bucket_name
  acl    = var.acl_value
tags = {
Name = "my-bucket"
}
}