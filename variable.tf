# list of variables


variable "region" {
    default = "us-east-1"
}

variable "access_key" {
}

variable "secret_key" { 
}


variable "bucket_name" {
}

variable "acl_value" {
    default = "private"
}


variable "iamuser" {
}


variable "admin_group" {
  default = "Admin"
}

variable "developer_group" {
  default = "Developer"
}

variable "readonly_group" {
  default = "ReadOnly"
}


variable "instance_type" {
    default = "t2.micro"
}

variable "ami_type" {
    type = string
    default = "ami-033b95fb8079dc481"
}

variable "role_type" {
    type = string
    default = "s3"
}

variable "user_name" {
    type = string
    default = "Developer"
}

variable "user_name2" {
    type = string
    default = "Admin"
}

variable "user_name3" {
    type = string
    default = "ReadOnly"
}

variable "s3_policy_name" {
    type = string
    default = "all_policy"
}

variable "ec2_role" {
    type = string
    default = "ec2_instance_role12"
}

variable "profile" {
    type = string
    default = "ec2_instance_profile"
}

variable "subscriber_email_addresses" {
}

variable "awsaccountid" {
}

variable "account_name" {
default = "monthlycost"
}

variable "account_budget_limit" {
}

# variable "key_pair" {
#    default = "mykey"
# }