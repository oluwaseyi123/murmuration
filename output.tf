# make the s3 policy information available on the command line

output "s3_policy" {
  value = data.aws_iam_policy_document.s3_policy.json
}